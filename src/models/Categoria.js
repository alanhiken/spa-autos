import Model from "./Model.js";

export default class Categoria extends Model {

    constructor(ApiConnector, DBConnector) {
        super("categoria", ApiConnector, DBConnector);
    }

}
