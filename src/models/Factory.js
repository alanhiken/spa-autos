import Api from "../core/Api";

import User from "./User";
import Vehiculo from "./Vehiculo";
import Categoria from "./Categoria";

var handler = {
    get: function (target, name) {
        if (name in target) {
            return target[name];
        }
        alert('No has definido un Modelo para: ' + name);
        return null;
    }
};

var Factory = new Proxy({}, handler);
Factory.api = Api;
Factory.user = new User(Api, null);
Factory.vehiculo = new Vehiculo(Api,null)
Factory.categoria = new Categoria(Api, null);
export default Factory;
