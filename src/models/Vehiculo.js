import Model from "./Model.js";

export default class Vehiculo extends Model {

    constructor(ApiConnector, DBConnector) {
        super("vehiculo", ApiConnector, DBConnector);
    }

    getbyId(id) {
        return this.post("getbyId", id);
    }

}
