import {createRouter, createWebHistory}  from 'vue-router'

const routes = [
    {
        path:'/',
        name:"login",
        meta: { rol: 'publico' }, 
        component:()=>import('./components/pages/Login.vue')
    },
    {
        path:'/home',
        meta: { rol: 'admin' }, 
        name:"Home",
        component:()=>import('./components/pages/Home.vue')
    },
    {
        path:'/autos',
        meta: { rol: 'admin' },
        name:"autos",
        component:()=>import('./components/pages/Autos.vue')
    },
    {
        path:'/autos/agregar',
        meta: { rol: 'admin' },
        name:"autosagregar",
        component:()=>import('./components/pages/AgregarAutos.vue')
    },
    {
        path:'/ver/bd',
        meta: { rol: 'admin' },
        name:"verbd",
        component:()=>import('./components/pages/Basededatos.vue')
    },
]

const router = createRouter({
    history:createWebHistory(),
    routes
})


function isLoggedIn() {
    //forma en que validas que estas logeado
    let token = localStorage.getItem('token');
    return token !== null && token.length > 0;
}

function rol() {
    return localStorage.getItem('rol');
}

router.beforeEach((to, from, next) => {
    if (to.meta.rol == 'publico') {
        console.log("publico");
        next();
    } else if (isLoggedIn()) {
        if (rol() == to.meta.rol) {
            console.log("logeado");
            next();
        } else {
            next(false);
        }
    } else {
        next({ name: 'Home' });
    }
});
export default router